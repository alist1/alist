default:
	go build -o bin/alist main.go

run:
	go run main.go server

webd:
	cd web && make

linux:
	CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o bin/alist-linux-amd64 main.go

linux/arm:
	CGO_ENABLED=1 GOOS=linux GOARCH=arm64 go build -o bin/alist-linux-arm64 main.go

linux/risc:
	CGO_ENABLED=1 GOOS=linux GOARCH=riscv64 CC=riscv64-linux-gnu-gcc \
	go build -o bin/alist-linux-riscv64 main.go

windows:
	CGO_ENABLED=1 GOOS=windows GOARCH=amd64 CC=x86_64-w64-mingw32-gcc-win32 \
	go build -o bin/alist-windows-amd64.exe -v main.go

# not support
wasm:
	# GOOS=wasip1 GOARCH=wasm go build -o alist.wasm main.go
	GOOS=js GOARCH=wasm go build -o bin/alist.wasm main.go
